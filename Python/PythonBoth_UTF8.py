# -*- coding: utf-8 -*-

import codecs

my_file = codecs.open("utf16-be-with-bom.txt", encoding="utf-16")
for i, line in enumerate(my_file):
    print("Line " + str(i) + " of length " + str(len(line)))