# -*- coding: utf8 -*-

import unicodedata

print("Python 3.x, in file with UTF-8 encoding")
# Based on https://docs.python.org/3/howto/unicode.html

def test_str(string, message):
    print(message)
    print(type(string))
    for i, c in enumerate(string):
        print(f"  [{i:02}] U+{ord(c):06X} {unicodedata.name(c, '')}")

def test_bytes(bytes, message):
    print(message)
    print (type(bytes))
    for i, c in enumerate(bytes):
        print(f"  [{i:02}] 0x{c:02X} {c:c}")

test_str("Hello 🐍", "default string type (str) in Python 3 is Unicode code points")
test_str("Pok\u00E9mon", "\\u and \\U escapes evaluated in string literals")
test_str("Pok\xE9mon", "\\x escapes evaluated in string literals, the results are by code point and are consistent")
test_str("\N{ROMAN NUMERAL EIGHT}", "use the name of the characters in an \\N escape")

résumé = "Non-ASCII allowed in identifiers, too"
print(résumé)

# Byte literals

test_bytes(b"ASCII-only\x80", "hex escapes processed")

# Converting bytes to str (decoding)

test_str(b"\xE2\x82\xAC".decode("utf-8"), "decode bytes using a codec name")
test_str(b"\xD8\x3C\xDF\x66".decode("utf-16-be"), "decode bytes using a codec name")
test_str(b"\xFE\xFF\xD8\x3C\xDF\x66".decode("utf-16"), "decode bytes using a codec name")

print("Decode strict...")
try:
    b"\x80Hello".decode("utf-8", "strict")
except Exception as e:
    print(f"  Caught {type(e)}: {e}")

test_str(b"\x80Hello".decode("utf-8", "ignore"), "decode ignore")
test_str(b"\x80Hello".decode("utf-8", "replace"), "decode replace")

# Converting str to bytes (encoding)

test_bytes("€".encode("utf-8"), "encode to utf-8")

print("Encode strict...")
try:
    "€5".encode("ascii", "strict")
except Exception as e:
    print(f"  Caught {type(e)}: {e}")

test_bytes("€5".encode("ascii", "ignore"), "encode with ignore")
test_bytes("€5".encode("ascii", "replace"), "encode with replace (replace with ?)")
test_bytes("€5".encode("ascii", "xmlcharrefreplace"), "encode with xmlcharrefreplace (replace with XML-style escape)")
test_bytes("€5".encode("ascii", "backslashreplace"), "encode with backslashreplace (replace with backslash escape)")
test_bytes("€5".encode("ascii", "namereplace"), "encode with namereplace (replace with Unicode name escape)")

# Codecs

import codecs

my_file_name = 'utf16-be-with-bom.txt'
my_file = codecs.open(my_file_name, encoding="utf-16")

print (f"Reading {my_file_name}...")
for i, line in enumerate(my_file):
    test_str(line, "Line %i" % i)