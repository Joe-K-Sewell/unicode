# -*- coding: utf-8 -*-

import unicodedata

print "Python 2.x, in file with ASCII encoding"
# Based on https://docs.python.org/2/howto/unicode.html

def test_str(string, message):
    print message
    print type(string)
    for i, c in enumerate(string):
        print "  [%02i] %s" % (i, c)
    print len(string)

def test_unicode(string, message):
    print message
    print type(string)
    for i, c in enumerate(string):
        print "  [%02i] U+%06X %s" % (i, ord(c), unicodedata.name(c, ""))
    print len(string)

def test_err(func, message):
    print message
    try:
        func()
    except Exception as e:
        print "  Caught %s: %s" % (type(e), e)

# Using str literals

test_str("Hello", "default string type in Python 2 is 8-bit str")
test_str("Pok\u00E9mon", "\\u and \\U escapes not evaluated in str")
test_str("Pok\x82mon", "you can use \\x escapes to get above 0x7F, but the results are codepage-dependent")

# Using unicode literals

test_unicode(u"Hello", "prepend u to string to make it of type unicode")
test_unicode(u"Pok\u00E9mon", "\\u and \\U are evaluated for unicode")
test_unicode(u"€ 4.99", "example using \\u for non-Latin-1 BMP character")
test_unicode(u"\U0001F366 yum!", "example using \\U for supplemental character")

# Using the unicode() constructor

test_unicode(unicode("Hello There"), "unicode constructor defaults to ASCII")

def x(): unicode("ABC \x80 DEF")
test_err(x, "unicode constructor defaults to throwing on invalid input")

test_unicode(unicode("ABC \x80 DEF", errors="replace"), "replace invalid input")

test_unicode(unicode("ABC \x80 DEF", errors="ignore"), "ignore invalid input")

test_unicode(unicode("\xE2\x82\xAC", encoding="UTF-8"), "unicode constructor using UTF-8 encoding")
test_unicode(unicode("\x3C\xD8\x66\xDF", encoding="UTF-16LE"), "unicode constructor using UTF-16 little endian encoding")
test_unicode(unicode("\xD8\x3C\xDF\x66", encoding="UTF-16BE"), "unicode constructor using UTF-16 big endian encoding") 

# Codecs

import codecs

my_file_name = 'utf16-be-with-bom.txt'
my_file = codecs.open(my_file_name, encoding="utf-16")

print "Reading {}...".format(my_file_name)
for i, line in enumerate(my_file):
    test_unicode(line, "Line %i" % i)