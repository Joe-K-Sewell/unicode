﻿using Mono.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text; // import this to get Encoding classes

namespace CSharpApp
{
    class Program
    {
        static void CharStringTest()
        {
            // C# supports Unicode "char"s...
            char basicChar = '€';         // using character in source
            char basicCharEsc = '\u20AC'; // using \u + 4 digit code point
            Debug.Assert(basicChar == basicCharEsc);

            // ...oops, what C# calls a "char" is actually a UTF-16 code unit.
            // So emoji (and other supplemental characters) can't fit in a char.
            // Uncommenting the following line causes a compiler error.
            //      char emojiChar = '🍦';

            // Strings can accept supplemental characters, but they're encoded
            // as two chars, just as in UTF-16. The count/indexing reflects this.
            string emojiString = "🍦";            // using character in source
            string emojiStringEsc = "\U0001F366"; // using \U + 8 digit code point
            Debug.Assert(string.Equals(emojiString, emojiStringEsc, StringComparison.OrdinalIgnoreCase));
            Debug.Assert(emojiString.Length == 2);
            Debug.Assert(emojiString[0] == '\uD83C');
            Debug.Assert(emojiString[1] == '\uDF66');
        }

        static void EncodingTest()
        {
            // System.Text.Encoding and subclasses can be used to encode/decode from strings
            Encoding defaultUtf8Encoding = Encoding.UTF8;
            byte[] euroBytes = defaultUtf8Encoding.GetBytes("€");
            Debug.Assert(euroBytes.Length == 3);
            Debug.Assert(euroBytes[0] == 0xE2);
            Debug.Assert(euroBytes[1] == 0x82);
            Debug.Assert(euroBytes[2] == 0xAC);

            // Can pass encodings to StreamWriter, StreamReader, etc.
            var emojiBytes = new byte[] { 0xF0, 0x9F, 0x8D, 0xA6 };
            using (var memoryStream = new MemoryStream(emojiBytes))
            {
                using (var readerWithEncoding = new StreamReader(memoryStream, defaultUtf8Encoding))
                {
                    var emojiString = readerWithEncoding.ReadToEnd();
                    Debug.Assert(emojiString == "🍦");
                }
            }

            // Caution, the static Encoding properties might not have the 
            // behavior you want. For instance, Encoding.UTF8 will insert a
            // byte order mark if you use it in a StreamWriter as a "preamble".
            // To write out UTF8 with no byte order mark, instantiate the UTF8Encoding
            // subclass yourself with the appropriate arguments.
            Encoding customUtf8Encoding = new UTF8Encoding(false);
            byte[] defaultUtf8Preamble = Encoding.UTF8.GetPreamble();
            Debug.Assert(defaultUtf8Preamble.Length == 3);
            byte[] customUtf8Preamble = customUtf8Encoding.GetPreamble();
            Debug.Assert(customUtf8Preamble.Length == 0);
        }

        static int Main(string[] args)
        {
            CharStringTest();
            EncodingTest();

            string inEncodingName = "utf8";
            bool inBigEndian = false;
            bool inBom = false;

            string outEncodingName = "utf8";
            bool outBigEndian = false;
            bool outBom = false;

            var encodingCreators = new Dictionary<string, Func<bool, bool, Encoding>>(StringComparer.OrdinalIgnoreCase)
            {
                { "ascii", (bigEndian, bom) => new ASCIIEncoding() },
                { "utf8", (bigEndian, bom) => new UTF8Encoding(bom) },
                { "utf16", (bigEndian, bom) => new UnicodeEncoding(bigEndian, bom) },
                { "utf32", (bigEndian, bom) => new UTF32Encoding(bigEndian, bom) },
            };

            var cliOptions = new OptionSet
            {
                { "in|inEncoding=", "encoding to use when reading (default: utf8)", a => inEncodingName = a },
                { "inBe|inBigEndian", "use big endian when reading? (default: false)", a => inBigEndian = a != null },
                { "inBom|inByteOrderMark", "assume a byte order mark when reading? (default: false)", a => inBom = a != null },

                { "out|outEncoding=", "encoding to use when writing (default: utf8)", a => outEncodingName = a },
                { "outBe|outBigEndian", "use big endian when writing? (default: false)", a => outBigEndian = a != null },
                { "outBom|outByteOrderMark", "use a byte order mark when writing? (default: false)", a => outBom = a != null },
            };

            var remainingArgs = cliOptions.Parse(args);

            if (remainingArgs.Count != 2)
            {
                Console.Error.WriteLine("usage: CSharpApp [options] inPath outPath");
                Console.Error.WriteLine("inPath: path to file to read");
                Console.Error.WriteLine("outPath: path to file to write");
                Console.Error.WriteLine("options:");
                cliOptions.WriteOptionDescriptions(Console.Error);
                Console.Error.WriteLine("encodings:");
                foreach (var kvp in encodingCreators)
                {
                    Console.Error.WriteLine($"\t{kvp.Key}");
                }
                return 2;
            }

            var inPath = remainingArgs[0];
            var outPath = remainingArgs[1];

            if (!encodingCreators.ContainsKey(inEncodingName))
            {
                Console.Error.WriteLine($"Unrecognized inEncoding {inEncodingName}");
                return 1;
            }
            var inEncoding = encodingCreators[inEncodingName](inBigEndian, inBom);

            if (!encodingCreators.ContainsKey(outEncodingName))
            {
                Console.Error.WriteLine($"Unrecognized outEncoding {outEncodingName}");
                return 1;
            }
            var outEncoding = encodingCreators[outEncodingName](outBigEndian, outBom);

            using (var inReader = new StreamReader(inPath, inEncoding))
            {
                using (var outWriter = new StreamWriter(outPath, false, outEncoding))
                {
                    var text = inReader.ReadToEnd();
                    outWriter.Write(text);
                }
            }
            
            return 0;
        }
    }
}
