package com.example;

import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.*;
import java.nio.file.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        charStringTest();
        if (args.length > 0) {
            charSetTest(args[0]);
        }
    }

    private static void charStringTest() {
        // This might look familiar to the C# sample...

        // Java supports Unicode "char"s...
        char basicChar = '€'; // using character in source
        char basicCharEsc = '\u20AC'; // using backslash u + 4 digit code point
        assert basicChar == basicCharEsc;

        // ...oops, what Java calls a "char" is actually a UTF-16 code unit.
        // So emoji (and other supplemental characters) can't fit in a char.
        // Uncommenting the following line causes a compiler error.
        // char emojiChar = '🍦';

        // Strings can accept supplemental characters, but they're encoded
        // as two chars, just as in UTF-16. The default count/indexing reflects this.
        String emojiString = "🍦"; // using character in source
        assert emojiString.length() == 2;
        assert emojiString.charAt(0) == '\uD83C';
        assert emojiString.charAt(1) == '\uDF66';

        // But you can get code point info using the UTF-16 indexing
        assert emojiString.codePointCount(0, 2) == 1;
        assert emojiString.codePointAt(0) == 0x1F366;

        // Unicode escapes are actually handled before comment stripping, so
        // so be careful: \u000A\u000DSystem.out.println("! This is printed even though it looks like a comment!");
    }

    private static void charSetTest(String filename) {
        // in java.nio.charset

        // Charset represents a character encoding.

        // StandardCharsets provides static fields for Charset instances
        // that are supported on every platform.
        Charset utf8 = StandardCharsets.UTF_8;
        Charset utf16 = StandardCharsets.UTF_16;

        // You can also get platform-specific encodings, e.g.:
        System.out.println("Available character sets are:");
        SortedMap<String, Charset> charsets = Charset.availableCharsets();
        for (Charset c : charsets.values()) {
            System.out.println("  " + c.name());
        }
        // Charset shiftJis = Charset.forName("Shift_JIS");

        // You can construct Strings from bytes and a charset
        String fromUtf8 = new String(new byte[] { (byte) 0xF0, (byte) 0x9F, (byte) 0x8D, (byte) 0xA6 }, utf8);
        assert fromUtf8.equals("🍦");
        assert fromUtf8.codePointAt(0) == 0x1F366;

        // You can also use charsets for reading files
        System.out.println("Reading from UTF-16 " + filename);
        try {
            try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename), utf16)){
                String readLine;
                while ((readLine = reader.readLine()) != null)
                {
                    for (int cp : readLine.codePoints().toArray())
                    {
                        System.out.println("  Code Point: " + cp);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
