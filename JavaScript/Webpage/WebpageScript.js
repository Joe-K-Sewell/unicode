// Encoding of this file is determined by byte order mark,
// then Content-Type header. For compatibility, libraries might
// want to stick to just ASCII and use \u escapes. See
// http://speakingjs.com/es5/ch24.html

function onLoad() {
    var output = ""

    function test(myStr, message) {

        function write(line) {
            output += line + "\n";
            console.log(line);
        }

        write(message);
        write("  " + myStr);
        write("  Length " + myStr.length);
        for (let i = 0; i < myStr.length; i++) {
            write(
                  "  [" 
                + i 
                + "] char " 
                + myStr.charCodeAt(i).toString(16) 
                + " codepoint " 
                + myStr.codePointAt(i).toString(16)
            );
        }
    }
    
    test("Hello", "Strings are UTF-16, so ASCII works by default")
    test("\uD83C\uDF66", "You can also use supplemental chars as escaped surrogate pairs")
    test("Pok\xE9mon", "Hex escape code point")
    test("\u{1F366}", "ECMAScript 6 introduces full codepoint escapes")

    document.getElementById("output").value = output;
}