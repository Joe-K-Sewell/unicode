// punycode is a process to encode Unicode in ASCII
// But it exposes some other helpful APIs
const punycode = require("punycode");

const assert = require('assert');
const fs = require("fs");

function test(myStr, message) {

    function write(line) {
        console.log(line);
    }

    write(message);
    write("  " + myStr);
    write("  Length " + myStr.length);
    for (let i = 0; i < myStr.length; i++) {
        write(
            "  [" 
            + i 
            + "] char " 
            + myStr.charCodeAt(i).toString(16) 
            + " codepoint " 
            + myStr.codePointAt(i).toString(16)
        );
    }
    
    var codepoints = punycode.ucs2.decode(myStr);
    write("  Codepoints Length " + codepoints.length);
    for (let i = 0; i < codepoints.length; i++) {
        const element = codepoints[i];
        write(
            "  [" 
            + i 
            + "] codepoint " 
            + element.toString(16)
        );
    }
}

test("Hello", "Strings are UTF-16/UCS-2, so ASCII works by default")
test("\uD83C\uDF66", "You can also use supplemental chars as escaped surrogate pairs")
test("Pok\xE9mon", "Hex escape code point")
test("\u{1F366}", "ECMAScript 6 introduces full codepoint escapes")

// Encodings are listed here:
// https://nodejs.org/api/buffer.html#buffer_buffers_and_character_encodings
var fileContent = fs.readFileSync("utf16-le-with-bom.txt", {
    encoding: 'utf-16le'
})
test(fileContent, "File read from disk");

var surrogates = "\uD83C\uDF66";
var unicodeEsc = "\u{1F366}";
assert(surrogates == unicodeEsc);
assert(surrogates.length == 2);
assert(unicodeEsc.length == 2);